﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashmashineTestApp
{
    public partial class Form1 : Form
    {
        private static ScannerControl s;
        public Form1()
        {
            InitializeComponent();
            s = new ScannerControl(this);
           
        }
        public RichTextBox GetRich()
        {
            return this.richTextBox1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread THR1 = new Thread(thread_work);
            s = new ScannerControl(this);
            THR1.Start();        
        }

        public static void thread_work()
        {
            s.start_transaction();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Thread THR2 = new Thread(thread_state_changer);
            THR2.Start();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Thread THR2 = new Thread(thread_state_changer_inhibit);
            THR2.Start();

        }
        public static void thread_state_changer_inhibit()
        {
            s.change_state(ControllerStates.IdlingCommand, ControllerData.DisableIdling);
        }
        public static void thread_state_changer()
        {
            s.change_state(ControllerStates.IdlingCommand,ControllerData.EnableIdling);
        }

        public RichTextBox getPrimljenNovac()
        {
            return PrimljenNovac;
        }
        public RichTextBox getOdbijenNovac()
        {
            return OdbijenNovac;
        }


    }
}
