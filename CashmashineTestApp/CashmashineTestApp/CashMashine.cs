﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;
using System.Xml;


namespace CashmashineTestApp
{
    public enum ControllerStates
    {
        StatusReq = 17,
        Ack = 80,
        Reset = 64,
        StackMinus1 = 65,
        IdlingCommand = 195
    }
    public enum ControllerData
    {
        NoData = -1,
        EnableIdling = 0,
        DisableIdling = 1
    }

    public class ScannerControl
    {
        
        //u ovom nizu se nalaze one komande kojima je potreban, ControllerData za njeno pokretanje
        public ControllerStates[] array_of_commands = new ControllerStates[]
        {
            ControllerStates.IdlingCommand
        };

        private System.IO.Ports.SerialPort mySerialPort;
        private static Mutex mut = new Mutex();

        private Form1 main_form = null;
        // [KONTROLER POSMATRAM KAO STATE MASINU]
        private ControllerStates State { get; set; }
        private ControllerData Command { get; set; }
        private int sequence_for_logger {get; set;}
        private Dictionary<int, int> moneyCommands { get; set; }
        private void set_serial_port_settings()
        {
            mySerialPort = new SerialPort();
            mySerialPort.PortName = "COM1";
            mySerialPort.BaudRate = 9600;
            mySerialPort.DataBits = 8;
            mySerialPort.StopBits = StopBits.One;
            mySerialPort.Parity = Parity.Even;
            mySerialPort.Handshake = Handshake.None;
        }
        private void initalize_money_dictionary()
        {
            moneyCommands = new Dictionary<int, int>();
            moneyCommands.Add(97,10);
            moneyCommands.Add(98,20);
            moneyCommands.Add(99,50);
            moneyCommands.Add(100,100);
            moneyCommands.Add(101,200);
            moneyCommands.Add(102,500);
            moneyCommands.Add(103,1000);
            moneyCommands.Add(104,2000);
        }
        public ScannerControl(Form1 f)
        {
            main_form = f;
            set_serial_port_settings();
            initalize_money_dictionary();

            State = ControllerStates.StatusReq;
            Command = ControllerData.NoData;
            
        }
        public void change_state(ControllerStates new_state, ControllerData data = ControllerData.DisableIdling)
        {
            mut.WaitOne();
            State = new_state;
            Command = data;
            mut.ReleaseMutex();
        }
        //Funkcija: Pomocna metoda, sluzi za zapis podataka u jednu od RichTextBox-ova!
        private void write_to_rich_text_box(int num, int money)
        {
            String s = "ESCROW ZA NOVCANICU -> ";
            s += moneyCommands[money].ToString();
            s += " din.";
            if(num==1)
            {
                if (main_form.InvokeRequired)
                {
                    main_form.Invoke(new Action(() =>
                    {
                        main_form.getPrimljenNovac().AppendText(s);
                    }));
                }
                else
                {
                    main_form.getPrimljenNovac().AppendText(s);
                }
            }
        }
        public void EscrowCommand(int money)
        {

            Console.WriteLine("Prepoznaje se ESCROW");
          
            if (State == ControllerStates.StatusReq)
                            State = ControllerStates.StackMinus1;

            write_to_rich_text_box(1,money);
        }
        public void AckCommand()
        {
            Console.WriteLine("Prepoznaje se AckFromController");

            if (State == ControllerStates.StackMinus1)
                State = ControllerStates.StatusReq;
        }
        public void VendValidCommand()
        {
            Console.WriteLine("Prepoznaje se Vend Valid");

            if (State == ControllerStates.StatusReq)
                            State = ControllerStates.Ack;
        }
        public void EnableCommand()
        {
            Console.WriteLine("Prepoznaje se IDLING");
            
            if (State == ControllerStates.Ack)
                            State = ControllerStates.StatusReq;
        }
        //Funkcija: [odgovora na enable/disable idling]
        //          [U ovom slucaju moramo obratiti paznju i na data podatak]
        public void IdlingCommandResponse(int data)
        {
            if(data == 0)
            {
                Console.WriteLine("Prepoznaje se ENABLE IDLING");
            }
            if (data == 0)
            {
                Console.WriteLine("Prepoznaje se *DISABLE IDLING");
            }
            if (State == ControllerStates.IdlingCommand)
                                State = ControllerStates.StatusReq;
            if (Command != ControllerData.NoData)
                                Command = ControllerData.NoData;

        
        }
        public void AcceptingCommand()
        {
            //Accepting komanda kaze da se zapocinje sekvence upisa u logger
            sequence_for_logger = 0;
            if(CashMashineControlFlows.StateMashineReceiveBills[sequence_for_logger] == AcceptorStatus.Accepting)
            {
                //append to logger to logger funkcija fali 
            }
        }

        private void StackedCommand()
        {
            Console.WriteLine("Prepoznaje se Stacked");
            if (State == ControllerStates.StatusReq)
                        State = ControllerStates.StatusReq;
        }


        //Funkcija: State changer function
        public void change_to_desired_state(List<byte> signal_for_state_changing)
        {
            //posmatra se samo treci bit da bi smo znali koja je vrsta signala
            if(signal_for_state_changing[1]==5)
            {
                switch (signal_for_state_changing[2])
                {
                    case (int)AcceptorStatus.Accepting: AcceptingCommand(); break;
                    case (int)AcceptorStatus.Escrow: EscrowCommand(signal_for_state_changing[1]); break;
                    case (int)AcceptorStatus.Ack: AckCommand(); break;
                    case (int)AcceptorStatus.VendValid: VendValidCommand(); break;
                    case (int)AcceptorStatus.Stacked: StackedCommand(); break;
                }
            }
            else
            {
                //ako se slala komanda sa podatkom u pravcu acceptor->kontroler on to treba da vrati samo
                switch (signal_for_state_changing[2])
                {
                    //6 bajtova potvrda za primljen IDLING COMMAND
                    case (int)ControllerStates.IdlingCommand: IdlingCommandResponse(signal_for_state_changing[3]); break; 
                    // [Potvrda o escrow-u neke novacinice]
                    case (int)AcceptorStatus.Escrow: EscrowCommand(signal_for_state_changing[3]); break;
                    case (int)AcceptorStatus.Ack: AckCommand(); break;
                }
            }
           

        }

        public void start_transaction()
        { 
            while(true)
            {           
                send_status();
            }
        }



        //Funkcija: Kreiraj komandu za upis u serijski port 
        //Nabrojana 3 preklopa
        //------------------------------------------------------------------------------
        public byte[] create_byte_array(int num1, int num2, int num3)
        { 
            byte[] arr =
               {
                    Convert.ToByte(num1), Convert.ToByte(num2), Convert.ToByte(num3),
                };
            return arr;
        }
        public byte[] create_byte_array(int num1, int num2 , int num3, int num4)
        {
            byte[] arr =
               {
                    Convert.ToByte(num1), Convert.ToByte(num2), Convert.ToByte(num3),Convert.ToByte(num4)
                };
            return arr;
        }
        public byte[] create_byte_array(byte[] arr, int num4 = 0, int num5 = 0)
        {
        if(arr.Length==3)
            {
              byte[] arr_final =
                {
                    arr[0],arr[1],arr[2], Convert.ToByte(num4), Convert.ToByte(num5),
                };
               return arr_final;
            }
        else
            {
                byte[] arr_final =
                {
                    arr[0],arr[1],arr[2],arr[3], Convert.ToByte(num4), Convert.ToByte(num5),
                };
                return arr_final;
            }

            
        }
        //------------------------------------------------------------------------------
        public void display_return_serialport_signal(List<byte> readData1)
        {
            Console.WriteLine("DISPLAY STATUS REQ");
            string s = "";
            foreach(var item in readData1)
            {
            
               s+= string.Format("{0:x}", item);         
               s += " ";
            }
            s += "\n";
            if (main_form.InvokeRequired)
            {
                main_form.Invoke(new Action(() =>
                {
                    main_form.GetRich().AppendText(s);
                    
                    if(readData1[1]==6)
                    {
                        int index = main_form.GetRich().Text.IndexOf(s);
                        int l = s.Length;
                        main_form.GetRich().Select(index, l);
                        main_form.GetRich().SelectionColor = Color.Red;
                    }
                    
                }));
            }
            else
            {
                main_form.GetRich().AppendText(s);

                if (readData1[1] == 6)
                {
                    int index = main_form.GetRich().Text.IndexOf(s);
                    int l = s.Length;
                    main_form.GetRich().Select(index, l);
                    main_form.GetRich().SelectionColor = Color.Red;
                }
            }
          
        }

        private byte[] create_command_upon_length()
        {
            Boolean sent_command=false;
            foreach(var item in array_of_commands)
            {
                if(item == State)
                {
                    sent_command = true;
                    break;
                }
            }
            if (sent_command)
            {
                Console.WriteLine("USO KURE");
                return create_byte_array(252, 6, (int)State,(int)Command);
            }else
            {
                Console.WriteLine("USO KURE");
                return create_byte_array(252, 5, (int)State);
            }
        }
        //Funkcija: na osnovu STATE-a controlera pravi se komanda koju je potrebno poslati kroz serijski port
        private byte[] create_command()
        {
           
            byte[] arr = create_command_upon_length();

            //nakon odredjene prva tri bajta potrebno je dopuniti comandu sa CRC-om
            // [hex addition kreiranje]
            string hex_addition = ComputeCRC(arr);
            // [string split]
            if (hex_addition.Length == 3)
                hex_addition = "0" + hex_addition;
            string first = hex_addition.Substring(0, 2);
            string second = hex_addition.Substring(2, 2);
            // [String parse to hex]
            int firstVal = int.Parse(first, System.Globalization.NumberStyles.HexNumber);
            int secVal = int.Parse(second, System.Globalization.NumberStyles.HexNumber);
            // [add to the end of the array]
            return create_byte_array(arr, firstVal, secVal);

        }

        public Boolean send_status()
        {
            List<byte> readData1 = new List<byte>();
            try
            {
                mut.WaitOne();

                mySerialPort.Open();
                mySerialPort.WriteTimeout = 200;
                mySerialPort.ReadTimeout = 100;

                byte[] arr = create_command();
                           

                mySerialPort.DiscardInBuffer();
                mySerialPort.DiscardOutBuffer();

                mySerialPort.Write(arr, 0, arr.Length);
                

              

                //posle slanja Ack komande ide se na status request, znaci ne gleda se povratni signal
                if (State == ControllerStates.Ack) 
                        State = ControllerStates.StatusReq;
                else
                {
                    readData1 = get_return_serialPort_signal(5, readData1);
                    change_to_desired_state(readData1);
                    display_return_serialport_signal(readData1);
                }
                           
               

                Thread.Sleep(200);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
            finally
            {
                mut.ReleaseMutex();
                mySerialPort.Close();
            }

            return true ;
        }
        //Funkcija: Metod prima signal koji vraca serijski port.
        //shouldRead parametar koji kaze koliko bajtova treba da vrati controller za odredjenu komandu
        public List<byte> get_return_serialPort_signal(int shouldRead, List<byte> readData1)
        {
             int read = 0;
             Boolean reading_started = false;
             Boolean reading_finished = false;

             while (read < shouldRead)
             {
                 int temp = mySerialPort.ReadByte();
                 // [citaj od prve komande 252, to je prva validna komanda]
                 if (temp == 252)
                 {
                     reading_started = true;
                     readData1.Add(Convert.ToByte(temp));
                     read++;
                 }
                 // [PROCITAJ 2. bajt]
                temp = mySerialPort.ReadByte();
                readData1.Add(Convert.ToByte(temp));
                read++;
                //2 bajta prva se svakako dodaju u signal, s stim sto 2. bajt odredjuje duzinu return signala

                if (temp == 5) shouldRead = 5;
                else if (temp == 6) shouldRead = 6;

                if (reading_started)
                 {
                     for (int i = 2; i < shouldRead; i++)
                     {
                         readData1.Add(Convert.ToByte(mySerialPort.ReadByte()));
                         read++;
                     }
                     reading_finished = true;
                 }
                 if (reading_finished)
                 {
                     break;
                 }

             }
           

            return readData1;
        }

        static String ComputeCRC(byte[] val)
        {
            long crc;
            long q;
            byte c;
            crc = 0;
            for (int i = 0; i < val.Length; i++)
            {
                c = val[i];
                q = (crc ^ c) & 0x0f;
                crc = (crc >> 4) ^ (q * 0x1081);
                q = (crc ^ (c >> 4)) & 0xf;
                crc = (crc >> 4) ^ (q * 0x1081);
            }
            long value= (byte)crc << 8 | (byte)(crc >> 8);
           
            string hex_value = string.Format("{0:x}", value);

            Console.WriteLine(hex_value);
            Console.WriteLine("******\n");

            return hex_value;
        }

       
    }
}
