﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CashmashineTestApp
{
    public enum AcceptorStatus
    {
        Idling = 17,
        Accepting = 18,
        Escrow = 19, //+DATA
        Stacking = 20,
        VendValid = 21,
        Stacked = 22,
        Rejecting = 23, //+DATA
        Returning = 24,
        Holding = 25,
        Inhibit = 26,
        Initalize = 27,
        Ack = 80,
    }
  
    public static class CashMashineControlFlows
    {
        public static AcceptorStatus[] StateMashineReceiveBills = new AcceptorStatus[]{
        AcceptorStatus.Idling,
        AcceptorStatus.Accepting,
        AcceptorStatus.Escrow,
        AcceptorStatus.Ack,
        AcceptorStatus.Stacking,
        AcceptorStatus.VendValid,
        AcceptorStatus.Stacked,
        AcceptorStatus.Idling
        };


    
    }
}
